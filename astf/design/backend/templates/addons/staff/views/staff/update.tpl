{if $member_data.member_id}
    {assign var="id" value=$member_data.member_id}
{else}
    {assign var="id" value=0}
{/if}

{capture name="mainbox"}



{*
                <div class="cm-sortable sortable-box" data-ca-sortable-table="images_links" data-ca-sortable-id-name="pair_id" id="additional_images">
                    {assign var="new_image_position" value="0"}
                    {foreach from=$member_data.image_pairs item=pair name="detailed_images"}
                        <div class="cm-row-item cm-sortable-id-{$pair.pair_id} cm-sortable-box">
                            <div class="cm-sortable-handle sortable-bar">
                                <img src="{$images_dir}/icon_sort_bar.gif" width="26" height="25" border="0" title="{__("sort_images")}" alt="{__("sort")}" class="valign" />
                            </div>
                            <div class="sortable-item">
                                {include file="common/attach_images.tpl" image_name="member_additional" image_object_type="product" image_key=$pair.pair_id image_type="A" image_pair=$pair icon_title=__("additional_thumbnail") detailed_title=__("additional_popup_larger_image") icon_text=__("text_additional_thumbnail") detailed_text=__("text_additional_detailed_image") delete_pair=true no_thumbnail=true}
                            </div>
                            <div class="clear"></div>
                        </div>
                        {if $new_image_position <= $pair.position}
                            {assign var="new_image_position" value=$pair.position}
                        {/if}
                    {/foreach}
                </div>
            {/if}

        <div id="box_new_image">
            <div class="clear cm-row-item">
                <input type="hidden" name="product_add_additional_image_data[0][position]" value="{$new_image_position}" class="cm-image-field" />
                <div class="image-upload-wrap pull-left">
                    {include file="common/attach_images.tpl" image_name="product_add_additional" image_object_type="product" image_type="A" icon_title=__("additional_thumbnail") detailed_title=__("additional_popup_larger_image") icon_text=__("text_additional_thumbnail") detailed_text=__("text_additional_detailed_image") no_thumbnail=true}
                </div>
                <div class="pull-right">
                    {include file="buttons/multiple_buttons.tpl" item_id="new_image"}
                </div>
             </div>
        </div>
*}

<form action="{""|fn_url}" method="post" name="update_member" class="form-horizontal form-edit "  enctype="multipart/form-data">
    




        <input type="hidden" name="member_id" value="{$id}" />

        <div class="control-group">
            <label class="control-label cm-required" for="elm_first_name">{__("stf_first_name")}</label>
            <div class="controls">
                <input type="text" id="elm_first_name" name="member_data[first_name]" size="35" value="{$member_data.first_name}" class="input-medium" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required" for="elm_last_name">{__("stf_last_name")}</label>
            <div class="controls">
                <input type="text" id="elm_last_name" name="member_data[last_name]" size="35" value="{$member_data.last_name}" class="input-medium" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required" for="elm_function">{__("stf_function")}</label>
            <div class="controls">
                <input type="text" id="elm_function" name="member_data[function]" size="35" value="{$member_data.function}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">{__("images")}:</label>
            <div class="controls">

                {include file="common/attach_images.tpl" image_name="staff_data" image_object_type="staff" image_pair=$member_data.image_pair no_thumbnail=true}
            </div>
        </div>


    <div class="control-group">
        <label class="control-label cm-required" for="elm_email">{__("stf_email")}</label>
        <div class="controls">
            <input type="text" id="elm_email" name="member_data[email]" size="35" value="{$member_data.email}" class="input-medium" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_position">{__("stf_position")}</label>
        <div class="controls">
            <input type="text" id="elm_position" name="member_data[position]" size="35" value="{$member_data.position}" class="input-medium" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label " for="elm_description">{__("stf_description")}</label>
        <div class="controls">
            <textarea id="elm_description" name="member_data[description]" cols="55" rows="8"  class="cm-wysiwyg input-large">
                {$member_data.description}
            </textarea>
        </div>
    </div>



    {include file="common/select_status.tpl" input_name="member_data[status]" id="member" obj=$member_data hidden=true}


{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="update_member" but_name="dispatch[staff.update]"}
    {else}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="update_member" save=$id}
    {/if}
{/capture}

</form>

{/capture}



{if !$id}
    {assign var="title" value=__("stf_add_member")}
{else}
    {assign var="title" value="{__("stf_editing_member")}: `$member.first_name`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}