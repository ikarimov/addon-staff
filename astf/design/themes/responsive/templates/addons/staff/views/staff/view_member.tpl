<style type="text/css">
.whole {
	padding-top: 80px;
}
td {
	padding: 5px;
	vertical-align: top;
}

div.member_pic {
	min-width: 180px;
	text-align: right;



}
div.member_pic img {
	margin: 5px;
    max-width: 200px;
    height: auto;
    vertical-align: top;


}
td.title {
	text-align: right;
	padding-right: 10px;
	font-weight: bold;
}

</style>



<div class="whole">
		<table>
			<tr>
				<td rowspan="4"><div class="member_pic">
					{include file="common/image.tpl" obj_id=$member_data.member_id images=$member_data.image_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}	
				</td></div>
				<td class="title">
					{__("stf_full_name")}:
				</td>
				<td class="value">
					{$member_data.first_name|truncate:32:"...":true} {$member_data.last_name|truncate:32:"...":true}
				</td>
			</tr>

			<tr>
				<td class="title">
					{__("stf_function")}:
				</td>
				<td class="value">
					{$member_data.function|truncate:48:"...":true}
				</td>
			</tr>

			<tr>
				<td class="title">
					{__("stf_email")}:
				</td>
				<td class="value">
					{$member_data.email|truncate:64:"...":true}
				</td>
			</tr>

			<tr>
				<td class="title descr">
					{__("stf_description")}:
				</td>
				<td class="value">
					{$member_data.description|truncate:48:"...":true}
				</td>
			</tr>
		</table>
</div>