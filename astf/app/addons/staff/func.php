<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }



function fn_get_staff($lang_code = DESCR_SL)
{
     $where = defined('RESTRICTED_ADMIN') ? "a.status != 'A' ": '1';

     $staff = db_get_array("SELECT 
                                a.member_id,
                                a.status, 
                                a.position,
                                a.email,
                                b.first_name, 
                                b.last_name,
                                b.function,
                                b.description
                            FROM ?:staff as a 
                            LEFT JOIN ?:staff_descriptions as b 
                            ON b.member_id = a.member_id AND b.lang_code = ?s 
                            WHERE $where 
                            ORDER BY first_name", $lang_code);
    return $staff;
}


function fn_get_member($member_id, $lang_code = DESCR_SL)
{    
     $member = db_get_row("SELECT 
                                a.member_id,
                                a.status, 
                                a.position,
                                a.email, 
                                b.first_name, 
                                b.last_name,
                                b.function,
                                b.description 
                            FROM ?:staff as a 
                            LEFT JOIN ?:staff_descriptions as b 
                            ON b.member_id = a.member_id AND b.lang_code = ?s 
                            WHERE a.member_id = $member_id
                            ORDER BY member_id", $lang_code);
    return $member;
}



function fn_update_member($member_data, $member_id, $lang_code = DESCR_SL)
{ 

    if (!empty($member_id)) {  
        db_query('UPDATE ?:staff SET ?u WHERE member_id = ?i', $member_data, $member_id);
        $member_data['member_id'] = $member_id;
        $member_data['lang_code'] = $lang_code;
        db_query('REPLACE INTO ?:staff_descriptions ?e', $member_data);
    } else {
        $member_id = db_query('INSERT INTO ?:staff ?e', $member_data);
        $member_data['member_id'] = $member_id;

        foreach (fn_get_translation_languages() as $member_data['lang_code'] => $v) {
            db_query("INSERT INTO ?:staff_descriptions ?e", $member_data);
        }

    }
    return $member_id;
}

function fn_delete_members($member_ids)
{
    db_query("DELETE FROM ?:staff WHERE member_id IN (?n)", $member_ids);
    db_query("DELETE FROM ?:staff_descriptions WHERE member_id IN (?n)", $member_ids);
}

