<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $suffix = '';

    //
    // Create/Update usergroups
  
    if ($mode == 'update') {
    	//fn_print_die($_REQUEST);
		
        $member_id = fn_update_member($_REQUEST['member_data'], 
        				 $_REQUEST['member_id'] );

        fn_attach_image_pairs('staff_data', 'staff', $member_id, DESCR_SL);

        $suffix .= ".update&member_id=" . $member_id;
    }



    if ($mode == 'delete') {

        if (!empty($_REQUEST['member_id'])) {

            fn_delete_members((array) $_REQUEST['member_id']);
        }

        return array(CONTROLLER_STATUS_REDIRECT, 'staff.manage');
    }

    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}




if ($mode == 'manage') {
 
    $staff = fn_get_staff();


    Tygh::$app['view']->assign('staff', $staff);



}elseif ($mode == 'update') {

	$staff = fn_get_member($_REQUEST['member_id']);

	$staff['image_pair'] = fn_get_image_pairs($_REQUEST['member_id'], 'staff', 'M', true, true, DESCR_SL);

	Tygh::$app['view']->assign('member_data', $staff);
}