<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

// View page details
//
if ($mode == 'view') {

    $staff = fn_get_staff();
    
    
   
   	$image_pairs = array();


   	
   	foreach ($staff as $key => $member) {

   		$staff[$key]['main_pair'] = fn_get_image_pairs($member['member_id'], 'staff', 'M');
   	}
   	

 

    Tygh::$app['view']->assign('staff', $staff);
    }
//


if ($mode == 'view_member') {

    $staff = fn_get_staff();
   
  	$member_data = fn_get_member($_REQUEST['member_id']);

	$member_data['image_pair'] = fn_get_image_pairs($_REQUEST['member_id'], 'staff', 'M');
    Tygh::$app['view']->assign('member_data', $member_data);
    }
//