<?php /* Smarty version Smarty-3.1.21, created on 2016-01-17 23:55:43
         compiled from "/opt/lampp/htdocs/shop/design/backend/templates/addons/staff/views/staff/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1488218570569bffcf619176-61457569%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52468861c3d950773112b17bf4f3b333fa162f8e' => 
    array (
      0 => '/opt/lampp/htdocs/shop/design/backend/templates/addons/staff/views/staff/update.tpl',
      1 => 1453029891,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1488218570569bffcf619176-61457569',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'member_data' => 0,
    'id' => 0,
    'member' => 0,
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_569bffcf7548b9_79291589',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569bffcf7548b9_79291589')) {function content_569bffcf7548b9_79291589($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('stf_first_name','stf_last_name','stf_function','images','stf_email','stf_position','stf_description','stf_add_member','stf_editing_member'));
?>
<?php if ($_smarty_tpl->tpl_vars['member_data']->value['member_id']) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['member_data']->value['member_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable(0, null, 0);?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>





<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="update_member" class="form-horizontal form-edit "  enctype="multipart/form-data">
    




        <input type="hidden" name="member_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />

        <div class="control-group">
            <label class="control-label cm-required" for="elm_first_name"><?php echo $_smarty_tpl->__("stf_first_name");?>
</label>
            <div class="controls">
                <input type="text" id="elm_first_name" name="member_data[first_name]" size="35" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required" for="elm_last_name"><?php echo $_smarty_tpl->__("stf_last_name");?>
</label>
            <div class="controls">
                <input type="text" id="elm_last_name" name="member_data[last_name]" size="35" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required" for="elm_function"><?php echo $_smarty_tpl->__("stf_function");?>
</label>
            <div class="controls">
                <input type="text" id="elm_function" name="member_data[function]" size="35" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['function'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo $_smarty_tpl->__("images");?>
:</label>
            <div class="controls">

                <?php echo $_smarty_tpl->getSubTemplate ("common/attach_images.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_name'=>"staff_data",'image_object_type'=>"staff",'image_pair'=>$_smarty_tpl->tpl_vars['member_data']->value['image_pair'],'no_thumbnail'=>true), 0);?>

            </div>
        </div>


    <div class="control-group">
        <label class="control-label cm-required" for="elm_email"><?php echo $_smarty_tpl->__("stf_email");?>
</label>
        <div class="controls">
            <input type="text" id="elm_email" name="member_data[email]" size="35" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['email'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_position"><?php echo $_smarty_tpl->__("stf_position");?>
</label>
        <div class="controls">
            <input type="text" id="elm_position" name="member_data[position]" size="35" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['position'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label " for="elm_description"><?php echo $_smarty_tpl->__("stf_description");?>
</label>
        <div class="controls">
            <textarea id="elm_description" name="member_data[description]" cols="55" rows="8"  class="cm-wysiwyg input-large">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member_data']->value['description'], ENT_QUOTES, 'UTF-8');?>

            </textarea>
        </div>
    </div>



    <?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"member_data[status]",'id'=>"member",'obj'=>$_smarty_tpl->tpl_vars['member_data']->value,'hidden'=>true), 0);?>



<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit-link",'but_target_form'=>"update_member",'but_name'=>"dispatch[staff.update]"), 0);?>

    <?php } else { ?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[staff.update]",'but_role'=>"submit-link",'but_target_form'=>"update_member",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

    <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

</form>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_smarty_tpl->__("stf_add_member"), null, 0);?>
<?php } else { ?>
    <?php ob_start();
echo $_smarty_tpl->__("stf_editing_member");
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars["title"] = new Smarty_variable($_tmp1.": ".((string)$_smarty_tpl->tpl_vars['member']->value['first_name']), null, 0);?>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['title']->value,'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
