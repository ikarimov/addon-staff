<?php /* Smarty version Smarty-3.1.21, created on 2016-01-18 14:46:37
         compiled from "/var/www/html/shop2/design/themes/responsive/templates/addons/staff/views/staff/view_member.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1794683820569ccc97ac0ac1-89232685%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d5784c6d5304ecd4bd9edec2ebb3eb330ec574a' => 
    array (
      0 => '/var/www/html/shop2/design/themes/responsive/templates/addons/staff/views/staff/view_member.tpl',
      1 => 1453117524,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1794683820569ccc97ac0ac1-89232685',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_569ccc97b9fad2_16570954',
  'variables' => 
  array (
    'runtime' => 0,
    'member_data' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569ccc97b9fad2_16570954')) {function content_569ccc97b9fad2_16570954($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/var/www/html/shop2/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/var/www/html/shop2/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('stf_full_name','stf_function','stf_email','stf_description','stf_full_name','stf_function','stf_email','stf_description'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><style type="text/css">
	.whole {
		padding-top: 80px;
	}
	td {
		padding: 5px;
		vertical-align: top;
	}

	div.member_pic {
		min-width: 180px;
		text-align: right;



	}
	div.member_pic img {
		margin: 5px;
	    max-width: 200px;
	    height: auto;
	    vertical-align: top;


	}
	td.title {
		text-align: right;
		padding-right: 10px;
		font-weight: bold;
	}
</style>



<div class="whole">
		<table>
			<tr>
				<td rowspan="4"><div class="member_pic">
					<?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['member_data']->value['member_id'],'images'=>$_smarty_tpl->tpl_vars['member_data']->value['image_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>
	
				</td></div>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_full_name");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['first_name'],24,"...",true), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['last_name'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_function");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['function'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_email");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['email'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title descr">
					<?php echo $_smarty_tpl->__("stf_description");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['description'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>
		</table>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/staff/views/staff/view_member.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/staff/views/staff/view_member.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><style type="text/css">
	.whole {
		padding-top: 80px;
	}
	td {
		padding: 5px;
		vertical-align: top;
	}

	div.member_pic {
		min-width: 180px;
		text-align: right;



	}
	div.member_pic img {
		margin: 5px;
	    max-width: 200px;
	    height: auto;
	    vertical-align: top;


	}
	td.title {
		text-align: right;
		padding-right: 10px;
		font-weight: bold;
	}
</style>



<div class="whole">
		<table>
			<tr>
				<td rowspan="4"><div class="member_pic">
					<?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['member_data']->value['member_id'],'images'=>$_smarty_tpl->tpl_vars['member_data']->value['image_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>
	
				</td></div>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_full_name");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['first_name'],24,"...",true), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['last_name'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_function");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['function'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title">
					<?php echo $_smarty_tpl->__("stf_email");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['email'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>

			<tr>
				<td class="title descr">
					<?php echo $_smarty_tpl->__("stf_description");?>
:
				</td>
				<td class="value">
					<?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['member_data']->value['description'],32,"...",true), ENT_QUOTES, 'UTF-8');?>

				</td>
			</tr>
		</table>
</div><?php }?><?php }} ?>
