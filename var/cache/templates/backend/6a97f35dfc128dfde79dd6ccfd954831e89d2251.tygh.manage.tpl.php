<?php /* Smarty version Smarty-3.1.21, created on 2016-01-18 14:07:55
         compiled from "/var/www/html/shop2/design/backend/templates/addons/staff/views/staff/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1124238470569cc78b1c8624-19105098%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a97f35dfc128dfde79dd6ccfd954831e89d2251' => 
    array (
      0 => '/var/www/html/shop2/design/backend/templates/addons/staff/views/staff/manage.tpl',
      1 => 1453110445,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1124238470569cc78b1c8624-19105098',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'hide_inputs' => 0,
    'staff' => 0,
    'member' => 0,
    'members' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_569cc78b2ccf77_13530740',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_569cc78b2ccf77_13530740')) {function content_569cc78b2ccf77_13530740($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/var/www/html/shop2/app/functions/smarty_plugins/function.script.php';
?><?php
fn_preload_lang_vars(array('stf_position','stf_full_name','stf_status','edit','delete','stf_no_items','stf_members_requests','stf_add_member','stf_members'));
?>
<?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>


<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <?php $_smarty_tpl->tpl_vars['hide_inputs'] = new Smarty_variable(fn_check_form_permissions(''), null, 0);?>
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="members_form" class="<?php if ($_smarty_tpl->tpl_vars['hide_inputs']->value) {?> cm-hide-inputs<?php }?>">
        <?php if ($_smarty_tpl->tpl_vars['staff']->value) {?>
            
            <table class="table table-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="15%"><?php echo $_smarty_tpl->__("stf_position");?>
</th>
                        <th width="10%"><?php echo $_smarty_tpl->__("stf_full_name");?>
</th>
                        <th width="1%">&nbsp;</th>
                        <th class="right" width="1%"><?php echo $_smarty_tpl->__("stf_status");?>
</th>
                    </tr>
                </thead>

                <?php  $_smarty_tpl->tpl_vars['member'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['member']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['staff']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['member']->key => $_smarty_tpl->tpl_vars['member']->value) {
$_smarty_tpl->tpl_vars['member']->_loop = true;
?>
                <tr class="cm-row-status-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['member']->value['member_id'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                    <td width="1%">
                        
                    </td>
                    <td class="row-name">
                        <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member']->value['position'], ENT_QUOTES, 'UTF-8');?>
</span>
                    </td>
                    <td class="row-name">
                        <a class="row-status cm-external-click" href="<?php echo htmlspecialchars(fn_url("staff.update&member_id=".((string)$_smarty_tpl->tpl_vars['member']->value['member_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member']->value['first_name'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['member']->value['last_name'], ENT_QUOTES, 'UTF-8');?>
</a>
                    </td>
                    <td >
                        <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                            <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>__("edit"),'href'=>"staff.update?member_id=".((string)$_smarty_tpl->tpl_vars['member']->value['member_id'])));?>
</li>
                            <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-confirm cm-post",'text'=>__("delete"),'href'=>"staff.delete?member_id=".((string)$_smarty_tpl->tpl_vars['member']->value['member_id'])));?>
</li>
                        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                        <div class="hidden-tools">
                            <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

                        </div>
                    </td>
                    <td class="nowrap right">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/select_popup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>$_smarty_tpl->tpl_vars['member']->value['member_id'],'status'=>$_smarty_tpl->tpl_vars['member']->value['status'],'hidden'=>true,'object_id_name'=>"member_id",'table'=>"staff"), 0);?>

                    </td>
                </tr>
                <?php } ?>
            </table>

        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("stf_no_items");?>
</p>
        <?php }?>
    </form>

    <?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
        <?php if (fn_check_view_permissions("members.update")) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>__("stf_members_requests"),'href'=>"staff.requests"));?>
</li>
                <?php if ($_smarty_tpl->tpl_vars['members']->value) {?>
                    <li class="divider"></li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[members.m_delete]",'form'=>"members_form"));?>
</li>
                <?php }?>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
           
        <?php }?>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"staff.add",'prefix'=>"top",'hide_tools'=>"true",'title'=>__("stf_add_member"),'icon'=>"icon-plus"), 0);?>

    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("stf_members"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
