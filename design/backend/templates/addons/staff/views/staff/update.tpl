{if $member_data.member_id}
    {assign var="id" value=$member_data.member_id}
{else}
    {assign var="id" value=0}
{/if}

{capture name="mainbox"}
    <form action="{""|fn_url}" method="post" name="update_member" class="form-horizontal form-edit "  enctype="multipart/form-data">
        <input type="hidden" name="member_id" value="{$id}" />
        <div class="control-group">
            <label class="control-label cm-required" for="elm_first_name">{__("stf_first_name")}</label>
            <div class="controls">
                <input type="text" id="elm_first_name" name="member_data[first_name]" size="35" value="{$member_data.first_name}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label cm-required" for="elm_last_name">{__("stf_last_name")}</label>
            <div class="controls">
                <input type="text" id="elm_last_name" name="member_data[last_name]" size="35" value="{$member_data.last_name}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label cm-required" for="elm_function">{__("stf_function")}</label>
            <div class="controls">
                <input type="text" id="elm_function" name="member_data[function]" size="35" value="{$member_data.function}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">{__("images")}:</label>
            <div class="controls">

                {include file="common/attach_images.tpl" image_name="staff_data" image_object_type="staff" image_pair=$member_data.image_pair no_thumbnail=true}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label cm-required cm-email" for="elm_email">{__("stf_email")}</label>
            <div class="controls">
                <input type="text" id="elm_email" name="member_data[email]" size="35" value="{$member_data.email}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="elm_position">{__("stf_position")}</label>
            <div class="controls">
                <input type="text" id="elm_position" name="member_data[position]" size="35" value="{$member_data.position}" class="input-medium" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label " for="elm_description">{__("stf_description")}</label>
            <div class="controls">
                <textarea id="elm_description" name="member_data[description]" cols="55" rows="8"  class="cm-wysiwyg input-large">
                    {$member_data.description}
                </textarea>
            </div>
        </div>

        {include file="common/select_status.tpl" input_name="member_data[status]" id="member" obj=$member_data hidden=true}

        {capture name="buttons"}
            {if !$id}
                {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="update_member" but_name="dispatch[staff.update]"}
            {else}
                {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="update_member" save=$id}
            {/if}
        {/capture}
    </form>
{/capture}

{if !$id}
    {assign var="title" value=__("stf_add_member")}
{else}
    {assign var="title" value="{__("stf_editing_member") }: `$member_data.first_name`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}