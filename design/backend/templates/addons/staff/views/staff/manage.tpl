{script src="js/tygh/tabs.js"}

{capture name="mainbox"}
    {$hide_inputs = ""|fn_check_form_permissions}
    <form action="{""|fn_url}" method="post" name="members_form" class="{if $hide_inputs} cm-hide-inputs{/if}">
        {if $staff}
            
            <table class="table table-middle">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="15%">{__("stf_position")}</th>
                        <th width="10%">{__("stf_full_name")}</th>
                        <th width="1%">&nbsp;</th>
                        <th class="right" width="1%">{__("stf_status")}</th>
                    </tr>
                </thead>

                {foreach from=$staff item='member'}
                <tr class="cm-row-status-{$member.member_id|lower}">
                    <td width="1%">
                        
                    </td>
                    <td class="row-name">
                        <span>{$member.position}</span>
                    </td>
                    <td class="row-name">
                        <a class="row-status cm-external-click" href="{"staff.update&member_id=`$member.member_id`"|fn_url}">{$member.first_name} {$member.last_name}</a>
                    </td>
                    <td >
                        {capture name="tools_list"}
                            <li>{btn type="list" text=__("edit") href="staff.update?member_id=`$member.member_id`"}</li>
                            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?member_id=`$member.member_id`"}</li>
                        {/capture}
                        <div class="hidden-tools">
                            {dropdown content=$smarty.capture.tools_list}
                        </div>
                    </td>
                    <td class="nowrap right">
                        {include file="common/select_popup.tpl" id=$member.member_id status=$member.status hidden=true object_id_name="member_id" table="staff"}
                    </td>
                </tr>
                {/foreach}
            </table>

        {else}
            <p class="no-items">{__("stf_no_items")}</p>
        {/if}
    </form>

    {capture name="buttons"}
        {if "members.update"|fn_check_view_permissions}
            {capture name="tools_list"}
                <li>{btn type="list" text=__("stf_members_requests") href="staff.requests"}</li>
                {if $members}
                    <li class="divider"></li>
                    <li>{btn type="delete_selected" dispatch="dispatch[members.m_delete]" form="members_form"}</li>
                {/if}
            {/capture}
           
        {/if}
    {/capture}

    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("stf_add_member") icon="icon-plus"}
    {/capture}
{/capture}

{include file="common/mainbox.tpl" title=__("stf_members") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}