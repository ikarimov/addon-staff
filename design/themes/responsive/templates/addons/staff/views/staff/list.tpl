{$staff|fn_print_r}

{if $member_data.member_id}
    {assign var="id" value=$member_data.member_id}
{else}
    {assign var="id" value=0}
{/if}



        <div class="control-group">
            <label class="control-label">{__("images")}:</label>
            <div class="controls">

                {include file="common/attach_images.tpl" image_name="staff_data" image_object_type="staff" image_pair=$member_data.image_pair no_thumbnail=true}
            </div>
        </div>