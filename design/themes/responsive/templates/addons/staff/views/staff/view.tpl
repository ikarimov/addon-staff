<style type="text/css">
    .gallery {
        padding-top: 55px;

    }
    div.gallery_pic {
     
        border: 1px solid #ccc;
        float: left;
        width: 150px;
        min-height: 150px;
        padding: 8px;
        margin-right: 6px;
        margin-bottom: 6px;
        text-align: center;
    }

    .gallery_pic:hover {
        border: 1px solid #777;
    }

    div.gallery_pic img {
        width: 100%;
        height: auto;
        vertical-align: top;
    }

    div.member_descr {
        
        text-align: center;
    }

    .member_descr .member_name {
        font-weight: bold;    
    }
</style>

<div id="ddd" class="gallery">
{foreach from=$staff item="staff_member" key="key"}
            <div class="gallery_pic">
                <a href="{"staff.view_member&member_id=`$staff_member.member_id`"|fn_url}">
                        {include file="common/image.tpl" obj_id=$key images=$staff_member.main_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}
                    <div class="member_descr">
                        <span class="member_name">{$staff_member.first_name|truncate:22:"...":true}
                        {$staff_member.last_name|truncate:22:"...":true}</span><br />
                        {$staff_member.function|truncate:22:"...":true}
                    </div>
                </a>
            </div>
{/foreach}
</div>

